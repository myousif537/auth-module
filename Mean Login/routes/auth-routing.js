const router = require('express').Router();

const authController = require('../controllers/auth-controller');
const bodyParser = require('body-parser')



router.post('/register', bodyParser.json(), authController.postSignUp);
router.post('/login', bodyParser.json(), authController.postLogin);
router.get('/profile',  authController.getUserData)








module.exports = router;