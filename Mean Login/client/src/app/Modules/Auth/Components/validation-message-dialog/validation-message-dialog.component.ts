import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-validation-message-dialog',
  templateUrl: './validation-message-dialog.component.html',
  styleUrls: ['./validation-message-dialog.component.css']
})
export class ValidationMessageDialogComponent implements OnInit {

  @Input('message') message

  constructor(public dialogRef: MatDialogRef<ValidationMessageDialogComponent>) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
