import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {ValidationMessageDialogComponent} from '../validation-message-dialog/validation-message-dialog.component'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AuthService} from  '../../Services/auth.service';
import {ITokenPayload} from '../../Interfaces/token-payload.interface';
import {Router, ActivatedRoute} from '@angular/router'






@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  credentials : ITokenPayload = {
    _id : 0,
    userName : '',
    email : '',
    password : '',
    isAdmin : false,
  }


  forms : NgForm[] = [];

  private _form : NgForm;

  @ViewChild('form', {static : false})
  set form(value : NgForm){
    this._form = value;

    if(this._form){
      this.forms.push(this._form)
    }
  }

  get form() : NgForm{
    return this._form
  }

  constructor(public dialog: MatDialog,
               private router : Router,
               private authServ : AuthService,
               private route : ActivatedRoute) { }

  ngOnInit() {
  }

  public validateForm(){
    let isValid = true;


    for(let form of this.forms){
      isValid = form.valid;

      if(!isValid){
        this.fireInvalidFormControls(form);
        return isValid;
      }
    }

    return isValid;
  }

  public fireInvalidFormControls(form : NgForm){
    if(!form) return;

    Object.keys(form.controls).forEach((key) => {
      if(!form.controls[key].valid)
          form.controls[key].markAsTouched();
    });
  }

  openValidationDialog(): void {
    const dialogRef = this.dialog.open(ValidationMessageDialogComponent,{
      width: '500px',

    },);

    dialogRef.afterClosed().subscribe(result => {

    });
  }
  login(){

    let isValid = this.validateForm();

    if(!isValid){
      let message = 'Email Address and Password is required'
      this.openValidationDialog();
      return;
    }
    else{
      this.authServ.login(this.credentials).subscribe(()=>{

        // this.authServ.isLoggedIn();
        // console.log(this.authServ.isLoggedIn())

        this.router.navigate(['/auth/profile'], {relativeTo : this.route})

      },err=>{
        //console.log(err);
        let message = err;
        this.openValidationDialog();
      })
    }
  }

}
