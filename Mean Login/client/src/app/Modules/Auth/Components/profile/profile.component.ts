import { Component, OnInit } from '@angular/core';
import {AuthService} from  '../../Services/auth.service';
import {IUser} from '../../Interfaces/user.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user : IUser;

  constructor(private authServ : AuthService) {

    this.authServ.profile().subscribe(user=>{
      this.user = user
    },err=>{
      //console.log(err);
    })
   }

  ngOnInit() {

  }

}
