import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import {HomeComponent,
        LoginComponent,
        RegisterComponent,
        ProfileComponent,
        AuthGuardService

} from './index'


const routes: Routes = [


  {
    path : '',
    children : [

      {
        path:'',
        component:  HomeComponent

      },

      {
        path : 'login',
        component : LoginComponent,

      },

      {
        path : 'signUp',
        component : RegisterComponent,
      },

      {
        path:  'profile',
        component : ProfileComponent,
        canActivate : [AuthGuardService]
      }





    ]
  }







];














@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }



 export const routedComponents = [HomeComponent,RegisterComponent,LoginComponent,ProfileComponent

]
