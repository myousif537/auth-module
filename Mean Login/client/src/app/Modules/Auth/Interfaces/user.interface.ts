export interface IUser {
    _id : Number;
    userName : string;
    email : string;
    password : string;
    created : Date;
    isAdmin : Boolean;
    exp : number;
    iat : number;
}
