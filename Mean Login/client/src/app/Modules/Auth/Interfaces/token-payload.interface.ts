export interface ITokenPayload{
  _id : number;
  userName : string;
  isAdmin : boolean;
  email : string;
  password : string;
}
