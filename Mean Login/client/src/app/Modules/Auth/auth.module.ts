import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { AuthRoutingModule, routedComponents} from './auth-routing.module';

import {HttpClientModule} from '@angular/common/http';

import {FormsModule} from '@angular/forms'

import {MaterialModule} from '../../material.module'

import {AuthService,
        AuthGuardService,
        ErrorHintPipe,
        ValidationMessageDialogComponent,
        LayoutComponent,
} from './index';

@NgModule({
  declarations: [
    routedComponents,
    ErrorHintPipe,
    ValidationMessageDialogComponent,
    LayoutComponent,
  ],
  imports: [

    AuthRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    MaterialModule,


  ],
  providers: [AuthService, AuthGuardService],
  entryComponents: [ValidationMessageDialogComponent]
})
export class AuthModule { }
