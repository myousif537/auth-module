import { Injectable } from "@angular/core";

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

import { Observable, of, from } from "rxjs";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

import { ITokenPayload, IUser, ITokenResponse } from "../index";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  iUser: IUser;
  iTokenRes: ITokenResponse;
  iTokenPayload: ITokenPayload;

  private token: string;

  apiBaseUrl = "http://localhost:3000";

  constructor(private httpClient: HttpClient, private router: Router) {
    //console.log(this.isLoggedIn())
  }

  private saveToken(token: string): void {
    localStorage.setItem("userToken", token);
    this.token = token;
  }

  private getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem("userToken");
    }
    return this.token;
  }

  public getUserDetails(): IUser {
    const token = this.getToken();
    let payload;
    if (token) {
      console.log(token);
      payload = token.split(".")[1];
      console.log(payload);

      payload = window.atob(payload);

      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public isLoggedIn(): boolean {
    var dateNow = new Date();

    const user = this.getUserDetails();

    //console.log(user)

    if (user) {
      if (user.exp > Date.now() / 1000) return true;
    } else {
      return false;
    }
  }

  public register(ITokenPayload: ITokenPayload): Observable<any> {
    return this.httpClient
      .request("post", this.apiBaseUrl + "/auth/register", {
        body: ITokenPayload
      })
      .pipe(
        map((res: ITokenResponse) => {
          if (res.token) {
            //this.saveToken(res.token);
            console.log(res.token);
          } else {
            return res;
          }
        })
      );
  }

  public login(ITokenPayload: ITokenPayload): Observable<any> {
    return this.httpClient
      .request("post", this.apiBaseUrl + "/auth/login", {
        body: ITokenPayload
      })
      .pipe(
        map((res: ITokenResponse) => {
          if (res.token) {
            console.log(res.token);
            this.saveToken(res.token);
          } else return res;
        })
      );
  }

  public profile(): Observable<any> {
    return this.httpClient
      .request("get", this.apiBaseUrl + "/auth/profile", {
        headers: new HttpHeaders().set("authorization", `${this.getToken()}`)
      })
      .pipe(
        map(res => {
          if (res) return res;
        })
      );
  }

  public logout(): void {
    console.log("Logout");
    this.token = "";
    window.localStorage.removeItem("userToken");
    this.router.navigate(["/auth/"]);
  }
}
