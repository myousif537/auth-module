import { Injectable } from '@angular/core';
import {Router, CanActivate} from '@angular/router'
import {AuthService} from '../auth.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authServ : AuthService, private router : Router) { }

  canActivate(){
    if(!this.authServ.isLoggedIn()){
      this.router.navigate(['/auth/']);
      return false;
    }
    else return true;
  }
}
