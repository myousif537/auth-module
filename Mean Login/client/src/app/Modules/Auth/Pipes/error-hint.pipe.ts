import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'errorHint'
})
export class ErrorHintPipe implements PipeTransform {

  constructor(){}

  transform(error: any, key? : string): any {
    if(error){
      if(key) return error[key];
      else{
        for(let k in error)
          return 'please fill this field'
      }
    }
    else{
      return '';
    }
  }

}
