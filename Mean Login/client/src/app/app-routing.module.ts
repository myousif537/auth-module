import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { from } from 'rxjs';


const routes: Routes = [

  {
    path : '',
    redirectTo : 'auth',
    pathMatch : 'full'
  },

  {
    path : 'auth',
    loadChildren : 'src/app/Modules/Auth/auth.module#AuthModule'
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
