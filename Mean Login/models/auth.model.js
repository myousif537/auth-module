const mongoose = require("mongoose");
const bCrypt = require("bcryptjs");

const dbUrl = "mongodb://localhost:27017/users";

const userSchema = mongoose.Schema({
  userName: String,
  email: String,
  password: String,
  created: {
    type: Date,
    default: Date.now()
  },

  iaAdmin: {
    type: Boolean,
    default: false
  }
});

const user = mongoose.model("user", userSchema);

exports.createUser = newUser => {
  //console.log(newUser)
  return new Promise((resolve, reject) => {
    mongoose
      .connect(dbUrl, { useUnifiedTopology: true, useNewUrlParser: true })
      .then(res => {
        user.findOne({ email: newUser.email }).then(userExisted => {
          if (userExisted) {
            mongoose.disconnect();
            reject("Email already exists");
          } else {
            bCrypt.hash(newUser.password, 10).then(hashedPassword => {
              newUser.password = hashedPassword;
              newUser.created = new Date();

              // console.log(newUser)

              let usr = new user({
                userName: newUser.userName,
                email: newUser.email,
                password: newUser.password,
                created: newUser.created,
                isAdmin: false
              });

              usr
                .save()
                .then(savedUser => {
                  console.log(savedUser);

                  mongoose.disconnect();
                  resolve(savedUser);
                })
                .catch(err => {
                  mongoose.disconnect();
                  reject(err);
                });
            });
          }
        });
      });
  });
};

exports.login = (email, password) => {
  console.log("Login");
  console.log(email, password);
  return new Promise((resolve, reject) => {
    mongoose
      .connect(dbUrl, { useUnifiedTopology: true, useNewUrlParser: true })
      .then(connected => {
        user.findOne({ email: email }).then(userExisted => {
          console.log(userExisted);
          if (!userExisted) {
            mongoose.disconnect();
            reject("Email Not Found");
          } else {
            bCrypt
              .compare(password, userExisted.password)
              .then(result => {
                if (!result) {
                  mongoose.disconnect();
                  reject("Password Is Incorrect");
                } else {
                  mongoose.disconnect();
                  resolve(userExisted);
                }
              })
              .catch(err => {
                mongoose.disconnect();
                reject(err);
              });
          }
        });
      });
  });
};

exports.fillUserData = userID => {
  return new Promise((resolve, reject) => {
    mongoose
      .connect(dbUrl, { useUnifiedTopology: true, useNewUrlParser: true })
      .then(connected => {
        user
          .findOne({ _id: userID })
          .then(userExisted => {
            if (!userExisted) {
              mongoose.disconnect();
              reject("User does not existed ");
            } else {
              mongoose.disconnect();
              resolve(userExisted);
            }
          })
          .catch(err => {
            mongoose.disconnect();
            reject(err);
          });
      });
  });
};
