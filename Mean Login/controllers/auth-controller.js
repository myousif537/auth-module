const auth = require('../models/auth.model');
const cors = require('cors');
const jwt = require('jsonwebtoken');


const jwtSecret = 'jwt is shortcut from json web token go ahead';



exports.postSignUp = (req, res, next)=>{

    //console.log("Indezzzz")

    let user = req.body;

    console.log(user)

    if(!user.userName || !user.password || !user.email){
        console.log('error')
        res.status(400);
        res.json({
            "error" : "Invalid Data"
        })
    }

    else{
        console.log("Indezzzz")
        auth.createUser(user).then(savedUser=>{
            console.log('Saved User')
            console.log(savedUser)
            let token = jwt.sign({
                userName : savedUser.userName,
                _id : savedUser._id,
                isAdmin : savedUser.isAdmin
            }, jwtSecret, {expiresIn : '1h'});
            
            res.json({token : token})
        }).catch(err=>{
            res.send('error : ' + err);
        })
    }
    
}


exports.postLogin = (req, res, next)=>{
    let user = req.body ;

    console.log(user)

    if(!user.email || !user.password){
        res.status(400);
        res.json({
            "error" : "Invalid Data"
        })
    }

    auth.login(user.email, user.password).then(userLogin=>{
        let token = jwt.sign({
            userID : userLogin._id,
            userName : userLogin.userName,
            isAdmin : userLogin.isAdmin
        },jwtSecret, {expiresIn : '1h'});
        console.log("Indezzzzzzz")
        console.log(userLogin)
        res.json({token : token})
    }).catch(err=>{
        res.send('error : ' + err)
    })
}

exports.getUserData = (req, res, next)=>{
    console.log(req.headers)
    let decoded = jwt.verify(req.headers['authorization'], jwtSecret);

    

    console.log(decoded);

    auth.fillUserData(decoded.userID).then(user=>{
        res.json(user);
    }).catch(err=>{
        res.send('error : ' + err)
    })

}
































