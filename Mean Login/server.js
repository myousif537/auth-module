const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const auth = require('./routes/auth-routing')

const port = 3000;


app.use((req, res, next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'authorization');
    next();
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
app.use(cors());
app.use('/auth', auth);






app.listen(port, ()=>{
    console.log('server listening on port ' + ' ' + port + ' Successfully')
})

















